This is the official [Kong](https://www.konghq.com) client for [Dr. Ashtetepe](https://drhttp.com/) service.

*Note: At the moment, as we are developing Dr. Ashtetepe, we ask you to use [Moesif](https://www.moesif.com/)'s [Kong library](https://github.com/Moesif/kong-plugin-moesif). Thanks to them to permit that via their [Apache licence](https://raw.githubusercontent.com/Moesif/kong-plugin-moesif/master/LICENSE)*. This README is a [TL;DR](https://en.wiktionary.org/wiki/tl;dr) for Dr.Ashtetepe usage.

# Installation
> [An integration example is provided here](https://bitbucket.org/drhttp/drhttp-kong/src/master/example/)

1) Retrieve a `dsn` ([Data source name](https://en.wikipedia.org/wiki/Data_source_name)) which can be found in [your project](https://drhttp.com/projects). eg: `https://<my_project_api_key>@api.drhttp.com/`

2) Follow [moesif documentation](https://github.com/Moesif/kong-plugin-moesif/blob/master/README.md) and make sure to add the `api_endpoint` configuration to point to `https://api.drhttp.com/moesif`. Moesif `application_id` is equivalent to DSN's `my_project_api_key`

    Example :
    ```
    curl -i -X POST --url http://localhost:8001/plugins/ \
    --data "name=moesif" \
    --data "config.application_id=YOUR_APPLICATION_ID";
    ```
    Becomes
    ```
    curl -i -X POST --url http://localhost:8001/plugins/ \
    --data "name=moesif" \
    --data "config.application_id=my_project_api_key" \
    --data "config.api_endpoint=https://api.drhttp.com/moesif";
    ```

### User identification

*Note: Device identification is not available yet in the kong library*

### Device identification

*Note: Device identification is not available yet in the kong library*

## Outbound request recording

*Note: Outbound request recording is not available yet in the kong library*

# Troubleshooting

Please [report any issue](https://bitbucket.org/drhttp/drhttp-kong/issues/new) you encounter concerning documentation or implementation. This is very much appreciated. We'll upstream the improvements to Moesif.
